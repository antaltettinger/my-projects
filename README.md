# Plugins developed by me and WP Core commits

## Plugin examples

1. Comment management plugin: Manage comments from a central location from across the multi site network.
2. Multi site search plugin: Customized for a specific use case, after correctly setting up it allows to search across the multi site network.

## WordPress core commits

https://github.com/Automattic/jetpack/pull/11585
https://github.com/Automattic/WP-Job-Manager/pull/1623
https://github.com/Automattic/sensei/pull/2342
https://github.com/Automattic/jetpack/pull/10664
https://github.com/Automattic/simplenote-electron/pull/1023