1. Make approve/disapprove buttons markup that submits the right data of the comment id and approval status to the server via admin_post.
2. Make a function that sets the comment approval status based on the clicked approval-disapproval button.
3. Make a WP_Comment Query that queries all the pending comments.
4. Make switch_to_blog function that will run the query, this is the main wrapper function that will run the query and rendering . this function is the function hooked to the plugin menu rendering.
5. Inside the switch_blog function run a nested foreach to render the comments with the approve-disapprove buttons.