<?php
/**
 * Plugin Name:       Comment Approval Plugin
 * Description:       Manage pending comments.
 * Version:           1.0
 * Author:            Antal
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       multi-site-comment-manager
 * 
 **/

namespace AntalTettinger\CommentApprovalPlugin;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

define( 'MULTI_SITE_COMMENT_MANAGER_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
//Require utility scripts
require_once( MULTI_SITE_COMMENT_MANAGER_PLUGIN_PATH . 'inc/functions/utility-scripts.php');
//Include all the functions for the hooks.
require_once( MULTI_SITE_COMMENT_MANAGER_PLUGIN_PATH . 'inc/functions/hook-scripts.php');

add_action('admin_menu', __NAMESPACE__ . '\comment_approval_settings_pages');
add_action( 'admin_post_update_approval', __NAMESPACE__ . '\approval_handling' );
add_action( 'wp_ajax_comment_approval', __NAMESPACE__ . '\comment_approval' );
add_action( 'wp_ajax_nopriv_comment_approval', __NAMESPACE__ . '\comment_approval' );
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\comment_approval_plugin_backend_scripts' );
add_action( 'admin_post_get_multi_site_comments', __NAMESPACE__ . '\get_multi_site_comments' );