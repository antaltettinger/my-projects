<?php 

?>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  
  <div class="wrap comment-approval-wrap container">
  <h1><?php esc_html_e(get_admin_page_title()); ?></h1>

  <form action="<?php echo admin_url('admin-post.php'); ?>" method="post">
  <div class="form-group">
   <input type="hidden" name="action" value="get_multi_site_comments">
   <input type="hidden" name="comments" value="1">
   <button type="submit" value="Get Comments" class="btn btn-primary">Get Comments</button>
   </div>
 </form>

 <?php if( htmlspecialchars($_GET["comments"]) == "get"): ?>

  <div id="sites-dropdown"></div>
  <div id="multi-site-comments"></div>
  <button id="load-more-comments" type="button" class="btn btn-primary btn-lg btn-block">Load more comments</button>
 <?php endif; ?>
  </div>