<div class="panel-group container border border-info rounded m-1" id="<?php echo($comment_panel_unique_id); ?>-panel">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h6 class="panel-title mt-2">
          <span class="panel-header" data-toggle="collapse" data-unique="<?php echo($comment_panel_unique_id); ?>"><span id="<?php echo($comment_panel_unique_id); ?>-title"> Site: <?php echo($blog_name);?> </span><strong> Article: <?php echo($post_title);?></strong></span>
        </h6>
      </div>
      <div id="<?php echo($comment_panel_unique_id); ?>" class="panel-collapse collapse show">
        <div class="panel-body">
        <span class="border border-primary"><a href=<?php echo esc_url($comment_post_url); ?> class="col" target="_blank">Link to post (ID: <?php echo($comment_post_id);?>)</a></span>
        <span class="border border-primary"><a href=<?php echo esc_url($comment_edit_url); ?> class="col" target="_blank">Edit Comment</a></span>
        <div class="row"><div class="col">Comment Date: <?php echo esc_attr($comment_date); ?></div></div>
        <div class="row"><div class="col">Comment Author IP: <?php echo esc_attr($comment_ip); ?></div></div>
        <div class="row"><div class="col">Comment Author Email: <?php echo esc_attr($comment_email); ?></div></div>
        <div class="comment-container--content row my-1">
            <blockquote class="col blockquote"><p class="mb-0"><?php echo esc_attr($comment_content); ?></p><footer class="blockquote-footer"><?php echo($comment_author);?></footer>
            </blockquote>
        </div>

        <div class="row"><div class="col"><?php echo($comment_star_rating);?></div></div>
  
          <div class="row mb-1">
            <div class="col">
                <button type="button" data-unique="<?php echo($comment_panel_unique_id); ?>" class="approval-button btn btn-success mr-1" data-comment-id="<?php echo($comment_id); ?>" data-blog-id="<?php echo($blog_id); ?>" data-approval="approve">
                Approve
                </button>
                <button type="button" data-unique="<?php echo($comment_panel_unique_id); ?>" class="approval-button btn btn-danger" data-comment-id="<?php echo($comment_id); ?>" data-blog-id="<?php echo($blog_id); ?>" data-approval="disapprove">
                Delete
                </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>