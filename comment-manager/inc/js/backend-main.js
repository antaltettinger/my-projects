document.addEventListener("DOMContentLoaded", function() {
  console.log(comment_approval_plugin_globals)
    const multiSiteCommentsElement = document.getElementById('multi-site-comments')
    const loadMoreCommentsButton = document.getElementById('load-more-comments')
    const commentDataArray = comment_approval_plugin_globals.comments_html_array
    let currentCommentDataArray = [...commentDataArray]
    let commentHtmlCurrentChunk = 0 , currentIndex = 0, untilIndex = 0

    displayDropDown()
    displayCommentsFromArray( currentCommentDataArray, commentHtmlCurrentChunk)
     
    loadMoreCommentsButton.addEventListener('click', event => {
      commentHtmlCurrentChunk+= 1
      console.log('Clicked ', commentHtmlCurrentChunk)
      displayCommentsFromArray( currentCommentDataArray, commentHtmlCurrentChunk)
    })

    document.getElementById('select-site').onchange = function() {
      console.log(commentDataArray)
      let selectedSite = this.value
      let newCommentDataArray = commentDataArray[0].filter(isSelectedSite, selectedSite)
      currentCommentDataArray[0] = newCommentDataArray
      commentHtmlCurrentChunk = 0
      currentIndex = 0
      untilIndex = 0
      multiSiteCommentsElement.innerHTML = '';
      displayCommentsFromArray( currentCommentDataArray, commentHtmlCurrentChunk )
  }

    document.querySelectorAll('.panel-header').forEach(item => {
        item.addEventListener('click', event => {
            const currentElement = event.currentTarget;
            const uniqueId = currentElement.dataset.unique;
            let [collapsibleElement] = getPanelElements(uniqueId);
            if(collapsibleElement.classList.contains('red')){
              return;
            }
            collapsibleElement.classList.toggle('show');
        })
    })
    document.querySelectorAll('.approval-button').forEach(item => {
        item.addEventListener('click', event => {
            const currentElement = event.currentTarget;
            const commentId = currentElement.dataset.commentId;
            const blogId = currentElement.dataset.blogId;
            const approvalStatus = currentElement.dataset.approval;
            const uniqueId = currentElement.dataset.unique;
            ajaxRequest(uniqueId, commentId, blogId, approvalStatus, comment_approval_plugin_globals);
            let [collapsibleElement,panelGroup, title] = getPanelElements(uniqueId);
            collapsibleElement.classList.toggle('show');
            panelGroup.classList.toggle('processing');
            title.innerHTML = "Processing...";
        })
      })

      function displayCommentsFromArray( currentCommentDataArray, commentHtmlCurrentChunk) {
        currentIndex = commentHtmlCurrentChunk*12
        untilIndex = currentIndex + 13
        currentDataArrayChunk = currentCommentDataArray[0].slice(currentIndex, untilIndex)
         currentDataArrayChunk.forEach(function(commentHtml) {
          const newDiv = document.createElement('div')
          newDiv.innerHTML = commentHtml[1]
          multiSiteCommentsElement.appendChild(newDiv)
        } )
    
      }

    function isSelectedSite(element) {
      if(element[0] == this) {
        return true;
      }
      return false;
    }

  });

  function ajaxRequest(uniqueId, commentId, blogId, approvalStatus, comment_approval_plugin_globals) {

    axios({
        method: 'post',
        url:  comment_approval_plugin_globals.ajax_url,
        params: {
          action: 'comment_approval',
          _ajax_nonce: comment_approval_plugin_globals.nonce,
          current_comment_id: commentId,
          current_blog_id: blogId,
          current_approval_status: approvalStatus,
          current_unique_id: uniqueId
        }
      })
      .then( function( response ) {
          const uniqueId = response.data.uniqueId;
        if( 'approve' == response.data.type ) {

            let [collapsibleElement, panelGroup,title] = getPanelElements(uniqueId);
            if (panelGroup.classList.contains('red')) {
                panelGroup.classList.remove('red');
            }
            panelGroup.classList.toggle('processing');
            panelGroup.classList.add('green');
            title.innerHTML = "Comment Approved - Click here to open panel for change";

        } else if('disapprove' == response.data.type){
            let [collapsibleElement, panelGroup, title] = getPanelElements(uniqueId);
            if (panelGroup.classList.contains('green')) {
                panelGroup.classList.remove('green');
            }
            panelGroup.classList.toggle('processing');
            panelGroup.classList.add('red');
            title.innerHTML = "Comment Deleted";
        } else {
            alert( 'Something went wrong, try logging in!' );
        }
      })
      .catch( function( error ) {
        alert( 'Something went wrong :(' );
        console.log( error );
      });
  }
  
  function getPanelElements(uniqueId){
    const panelUniqueId = uniqueId + '-panel';
    const titleId = uniqueId + '-title';
    const collapsibleElement = document.getElementById(uniqueId);
    const panelGroup =  document.getElementById(panelUniqueId);
    const title = document.getElementById(titleId);
    return [collapsibleElement, panelGroup, title];
  }

  function displayDropDown(){
    const sitesDropdown = document.getElementById('sites-dropdown')
    const newDiv = document.createElement('div')
    newDiv.innerHTML = comment_approval_plugin_globals.dropdown
    sitesDropdown.appendChild(newDiv)
  }