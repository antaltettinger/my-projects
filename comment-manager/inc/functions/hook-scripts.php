<?php

namespace AntalTettinger\CommentApprovalPlugin;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

use AntalTettinger\CommentApprovalPlugin\Multi_Site_Comments;

//Admin page settings
function comment_approval_settings_pages() {
  add_menu_page(
      __('Pending Comments', 'multi-site-comment-manager'),
      'Comment Management',
      'comment_manager',
      'comment-approval-plugin.php',
      __NAMESPACE__ . '\comment_approval_settings_page_markup',
      'dashicons-testimonial',
      100
  );
}

function comment_approval_settings_page_markup() {
  include ( MULTI_SITE_COMMENT_MANAGER_PLUGIN_PATH . 'inc/display/main-page.php');
}

//Load and localize JavaScript
function comment_approval_plugin_backend_scripts( $hook ) {
  if(!isset($_GET["comments"]) ) {
    return;
  }
$multi_site_commments = new Multi_Site_Comments();
$multi_site_comment_query_results = $multi_site_commments->get_comments();
$multi_site_dropdown = $multi_site_commments->get_dropdown_html();

    if( $hook != 'toplevel_page_' . 'comment-approval-plugin' ) {
      return;
    }
    $nonce = wp_create_nonce( 'comment_approval_plugin_nonce' );
  
    wp_enqueue_script( 'axios-js', 'https://unpkg.com/axios/dist/axios.min.js', [], '', true);
    wp_enqueue_script( 'comment-approval-plugin-backend-js', plugins_url( '../js/backend-main.js', __FILE__ ), ['axios-js'], filemtime( MULTI_SITE_COMMENT_MANAGER_PLUGIN_PATH . 'inc/js/backend-main.js'), true );
    wp_enqueue_style( 'comment-manager-css', plugins_url( '../css/comment-manager-plugin.css', __FILE__ ), array(), filemtime( MULTI_SITE_COMMENT_MANAGER_PLUGIN_PATH . 'inc/css/comment-manager-plugin.css') );
    wp_localize_script(
      'comment-approval-plugin-backend-js',
      'comment_approval_plugin_globals',
      [
        'ajax_url'    => admin_url( 'admin-ajax.php' ),
        'nonce'       => $nonce,
        'comments_html_array' => $multi_site_comment_query_results,
        'dropdown' => $multi_site_dropdown
      ]
    );
  }


function comment_approval() {
    global $wpdb;

    check_ajax_referer( 'comment_approval_plugin_nonce' );
    $comment_id = $_REQUEST['current_comment_id'];
    $blog_id = $_REQUEST['current_blog_id'];
    $approval_status = $_REQUEST['current_approval_status'];

    $comments_table = 'wp_' . $blog_id . '_comments';
    //$comments_db_query = "UPDATE {$comments_table} SET comment_approved = 1  WHERE comment_id='{$comment_id}'";
    if($approval_status == "approve") {
      $response['type'] = "approve";
      $wpdb->update( $comments_table, array( 'comment_approved' => 1 ), array('comment_id' => $comment_id), array("%d"));

    } elseif($approval_status == "disapprove") {
      switch_to_blog( $blog_id );
      $response['type'] = "disapprove";
      wp_delete_comment( $comment_id, true );
    }
    $response['uniqueId'] = $_REQUEST['current_unique_id'];

    $response = json_encode($response);
    echo $response;
  
    die();
  
  }

  function get_multi_site_comments() {
    wp_redirect(admin_url('admin.php?page=comment-approval-plugin.php&comments=get'));
  }
