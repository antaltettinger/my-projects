//Search Handling
const urlParams = new URLSearchParams(window.location.search);
const myParam = urlParams.get('q');
const siteBaseUrl = window.location.hostname;
let currentChunk = 0;
let chunkLength = 0;
let searchResults = [];
let numberOfResults = 0;
let currentResults = 0;


jQuery(document).ready(function($) {
  $('meta[name=description]').attr('content', `Search Results for: ${myParam}`);

  $(".archive-title").html(`Search Results for: ${myParam}`);

  if (myParam != null)
    {
      let searchUrlString = 'https://' + siteBaseUrl + '/wp-json/bestreviews/search?term=' + myParam;

      jQuery.getJSON( searchUrlString, (results) => {

        function checkProperties(obj) {
          for (var key in obj) {
              if (obj[key] !== null && obj[key] != "")
                  return false;
          }
          return true;
      }
        if(checkProperties(results) == false) {
        const concatResults = results.in_title_current_review.concat( results.in_title_review, results.in_title, results.reviews, results.others )
        searchResults = chunk( concatResults, 10 )
        numberOfResults = results.number_of_results
        chunkLength = searchResults.length
        renderSearchResults( searchResults, currentChunk, numberOfResults, chunkLength );
      } else {
        $("#bestreviews-search-results").html(`
        <div class="entry"><p>Sorry, no content matched your criteria.</p></div>
        `)
      }

      let nextClick = document.getElementById('bestreviews-search-next');
      let previousClick =  document.getElementById('bestreviews-search-previous');
      $(nextClick).click(goNextPage);
      $(previousClick).click(goPreviousPage);

      function renderSearchResults( searchResults, currentChunk, numberOfResults, chunkLength ) {
        currentResults = searchResults[currentChunk];
        console.log(currentResults)
      
        $("#bestreviews-search-results").html(`
        ${currentResults.map(item => createMapInner(item)).join('')}
      
        <div class="archive-pagination pagination">
      <ul role="navigation" aria-labelledby="paginglabel">
        ${(currentChunk == 0) ? ''
        : `<li class="pagination-next custom-search-list"><a href="#search-results-header" id="bestreviews-search-previous"><span class="screen-reader-text">« Go to Previous Page </span></a></li>`
        }
        ${(numberOfResults > 10 && (currentChunk <= (chunkLength-1))) ? 
          `<li class="pagination-next custom-search-list"><a id="bestreviews-search-next" href="#search-results-header"><span class="screen-reader-text">Go to Next Page »</span></a></li>`
        : ''
        }
      </ul>
      </div>
      `)
      }

      function goNextPage() {
        currentChunk+=1;
        renderSearchResults( searchResults, currentChunk, numberOfResults, chunkLength )
     
        let nextClick = document.getElementById('bestreviews-search-next');
        let previousClick =  document.getElementById('bestreviews-search-previous');
        $(nextClick).click(goNextPage);
        $(previousClick).click(goPreviousPage);
      }
      
      function goPreviousPage() {
        currentChunk-=1;
        renderSearchResults( searchResults, currentChunk, numberOfResults, chunkLength )

        let nextClick = document.getElementById('bestreviews-search-next');
        let previousClick =  document.getElementById('bestreviews-search-previous');
        $(nextClick).click(goNextPage);
        $(previousClick).click(goPreviousPage);
      }
      
      //Chunking the results array.
      function chunk (arr, len) {
      
        var chunks = [],
            i = 0,
            n = arr.length;
      
        while (i < n) {
          chunks.push(arr.slice(i, i += len));
        }
      
        return chunks;
      }
      
      //Create HTML for a particular article, from the returned search data.
      function createMapInner( item ) {
        if(item != undefined){
        return `
        <article>
        <header class="entry header">
        <h3 class="entry-title">
        <a class="entry-title-link" rel="bookmark" href="${item.permalink}">${item.title}</a>
        </h3>
        </header>
        <div class="entry-content">
          <p>
          ${item.imageUrl ? 
          `<a href="${item.imageUrl}">
          <img src="${item.imageUrl}" class="alignright size-medium">
          </a>` : ''}
              ${item.description}
              <a class="more-link" href="${item.permalink}" title="Read More">Read More…</a>
          </p>
        </div>
        </article>
        `;
      }
      }


    });
  }
});