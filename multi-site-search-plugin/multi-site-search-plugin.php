<?php
/**
 * Plugin Name:       Multi Site Search Plugin
 * Description:       Search across a multi site network.
 * Version:           1.00
 * Author:            Antal
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       multi-site-search-plugin
 * 
 **/

namespace AntalTettinger\MultiSiteSearchPlugin;

if ( ! defined( 'ABSPATH' ) ) exit;

//Register the multi site search 
add_action('rest_api_init', __NAMESPACE__ . '\register_search');

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\load_scripts' );

register_activation_hook( __FILE__ , __NAMESPACE__ .'\plugin_activation' );

function plugin_activation() {
  
  if ( ! current_user_can( 'activate_plugins' ) ) return;
  
  global $wpdb;
  
	$siteArray = get_sites();
	$post_id_array = array();
  foreach($siteArray as $subsite){
	$subsite_id = get_object_vars($subsite)["blog_id"];

	switch_to_blog( $subsite_id );
  
  if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'search-results'", 'ARRAY_A' ) ) {
	 
	
	$post_content = '<div class="archive-description">
	<h2 class="archive-title" id="search-results-header">Search Results</h2>
	</div>
	<div id="bestreviews-search-results">
	<div class="lds-ring"><div></div><div></div><div></div><div></div></div>
	</div>';

    // create post object
    $page = array(
      'post_title'  => __( 'Search Results' ),
      'post_status' => 'publish',
      'post_author' => 'Best Reviews',
	  'post_type'   => 'page',
	  'post_content' => $post_content
    );
    
    // insert the post into the database
	$post_id = wp_insert_post( $page );
	$post_id_array[] = $post_id;
  }
}
	$css_no_display_string = '';
	foreach($post_id_array as $post_id) {
		$array_element = '.page-id-' . $post_id . ' .entry-header .entry-title{
			display:none;} ';
		$css_no_display_string = $css_no_display_string  .  $array_element;
	}

	file_put_contents( plugin_dir_path( __FILE__ ) . 'css/title_no_display.css', $css_no_display_string, FILE_APPEND);

}

function load_scripts() {

	if ( is_page( 'search-results' ) ) {
		$plugin_dir = plugin_dir_url(__FILE__);

		wp_enqueue_script('multi_site_search_js', $plugin_dir  . 'js/custom_search.js', array('jquery'), time(), true);
		wp_register_style( 'multi_site_search_css', $plugin_dir . 'css/custom_search.css', false );
		wp_register_style( 'multi_site_search_css_title_no_display', $plugin_dir . 'css/title_no_display.css', false );
		wp_enqueue_style( 'multi_site_search_css' );
		wp_enqueue_style( 'multi_site_search_css_title_no_display' );

	//Shortcodes Ultimate dependencies.
		wp_register_style( 'search-su-icons', plugins_url() . '/shortcodes-ultimate/includes/css/icons.css');
		wp_enqueue_style( 'search-su-icons' );
		wp_register_style( 'search-su-css', plugins_url() . '/shortcodes-ultimate/includes/css/shortcodes.css');
		wp_enqueue_style( 'search-su-css' );
}

}

function register_search() {
	register_rest_route('bestreviews', 'search', array(
    'methods' => \WP_REST_SERVER::READABLE,
    'callback' => __NAMESPACE__ . '\results'
  ));
}

function results($data) {
	global $wp;
	$current_blog_id = get_current_blog_id();
	$search_array = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,19,23,24,25,26,27,28,29,30,31,33,34,35,36,38,39,40);
	$current_blog_location = array_search($current_blog_id, $search_array);
	unset($search_array[$current_blog_location]);
	array_unshift($search_array, $current_blog_id);
	$search_term_query = sanitize_text_field($data['term']);
	$search_term = str_replace('+', ' ', $search_term_query);
	$search_term = 'Medical+Alert';
	$number_of_results = 0;

	$results = array(
		'in_title_current_review' => array(),
		'in_title_review' => array(),
		'in_title' => array(),
		'reviews' => array(),
		'others' => array()
	  );

	//$offset = $data->get_param( 'offset' );

		foreach($search_array as $subsite){
			switch_to_blog( $subsite );
			//$cat_reviews = get_category_by_slug('reviews');
			$args = array(
				'no_found_rows' => true,
				'post_type' => array('post','page'),
				's' => $search_term,
				'post_status'=> 'publish',
				'ignore_sticky_posts' => true,
				'posts_per_page' => -1
			);
			$mainQuery = new \WP_Query($args);
			//coomm
			if($mainQuery->have_posts()) {
			  while($mainQuery->have_posts()) {
				$mainQuery->the_post();
				$permalink = get_the_permalink();
				$review_permalink = strpos($permalink, '-reviews');
				$title_contains_search_term = stripos($permalink, $search_term);

				//Get title and check if it has the search term
				$title = get_the_title();
				if( $title == 'Homepage' || $title == 'All Reviews') {
					continue;
				}
				// Fetch post content	
				$content = get_post_field( 'post_content', get_the_ID() );
				// Get content parts
				$shortcoded = do_shortcode($content);
				$content_parts = get_extended( $shortcoded );
				// Output part before <!--more--> tag
				$before_read_more =  $content_parts['main'];
				if (strpos($before_read_more, 'img') !== false) {
					$image = "";
				} else {
					$image = get_the_post_thumbnail_url(null, 'portfolio');
				}
				//substr(get_the_excerpt(), 0, -10)
				$current_array = array(
					'title' => $title,
					'permalink' => $permalink,
					'description' => $before_read_more,
					'imageUrl' => $image
				);

				if( $subsite == $current_blog_id && $title_contains_search_term && $review_permalink) {
					array_push($results['in_title_current_review'], $current_array);
				} else if($title_contains_search_term && $review_permalink){
					array_push($results['in_title_review'], $current_array);
				} else if($title_contains_search_term) {
					array_push($results['in_title'], $current_array);
				} else if($review_permalink){
					array_push($results['reviews'], $current_array);
				} else {
					array_push($results['others'], $current_array);
				}

				$number_of_results = $number_of_results + 1;
				
				}
			}
			wp_reset_query();

	}

	restore_current_blog();
	$results['number_of_results'] = $number_of_results;
	  return $results;
  
}